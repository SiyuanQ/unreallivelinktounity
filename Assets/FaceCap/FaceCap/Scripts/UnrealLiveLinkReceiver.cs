using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using UnityEngine;

namespace FortySevenE
{
    public struct LiveLinkTimeCode
    {
        public double Time;
        public double Offset;
    }

    public class FaceCaptureData
    {
        public List<float> blendshapeValues;
        public Quaternion headRotation;
        public Quaternion leftEyeRotation;
        public Quaternion rightEyeRotation;

        public FaceCaptureData(float[] ueLiveLinkDataPatch)
        {
            if (ueLiveLinkDataPatch.Length != 61)
            {
                Debug.LogError("Cannot parse UE LiveLink Data. Length does not match");
                return;
            }
            blendshapeValues = ueLiveLinkDataPatch.Take(52).ToList();
            blendshapeValues.Add(0); // add "none"

            var rotationArray = ueLiveLinkDataPatch.Skip(52).ToArray();
            //Yaw Pitch Row order
            headRotation = Quaternion.Euler(new Vector3(rotationArray[1], rotationArray[0], rotationArray[2]) * -90);
            leftEyeRotation = Quaternion.Euler(new Vector3(rotationArray[4], rotationArray[3], rotationArray[5]) * -90);
            rightEyeRotation = Quaternion.Euler(new Vector3(rotationArray[7], rotationArray[6], rotationArray[8])* -90);
        }
    }

    public class UnrealLiveLinkReceiver : MonoBehaviour
    {
        #region Blendshapes order
        private readonly string[] _ueLiveLinkDataPatch = { "eyeBlinkRight", "eyeLookDownRight", "eyeLookInRight", "eyeLookOutRight", "eyeLookUpRight", "eyeSquintRight", "eyeWideRight", "eyeBlinkLeft", "eyeLookDownLeft", "eyeLookInLeft", "eyeLookOutLeft", "eyeLookUpLeft", "eyeSquintLeft", "eyeWideLeft", "jawForward", "jawRight", "jawLeft", "jawOpen", "mouthClose", "mouthFunnel", "mouthPucker", "mouthRight", "mouthLeft", "mouthSmileRight", "mouthSmileLeft", "mouthFrownRight", "mouthFrownLeft", "mouthDimpleRight", "mouthDimpleLeft", "mouthStretchRight", "mouthStretchLeft", "mouthRollLower", "mouthRollUpper", "mouthShrugLower", "mouthShrugUpper", "mouthPressRight", "mouthPressLeft", "mouthLowerDownRight", "mouthLowerDownLeft", "mouthUpperUpRight", "mouthUpperUpLeft", "browDownRight", "browDownLeft", "browInnerUp", "browOuterUpRight", "browOuterUpLeft", "cheekPuff", "cheekSquintRight", "cheekSquintLeft", "noseSneerRight", "noseSneerLeft", "tongueOut", "HeadYaw", "HeadPitch", "HeadRoll", "LeftEyeYaw", "LeftEyePitch", "LeftEyeRoll", "RightEyeYaw", "RightEyePitch", "RightEyeRoll" };
        #endregion

        [Header("UDP Connection")]
        [SerializeField] private int _port = 11111;

        [Header("Decoding")]
        [SerializeField] private int _blendshapeCount = 61;
        [SerializeField] [Range(0, 324)] int _blendshapeByteOffset;
        [SerializeField] bool _flipEndianness = true;

        [Header("Debug")]
        [SerializeField] private bool _debugPrint = false;

        public event Action<FaceCaptureData> receivedFaceCaptureData;

        public FaceCaptureData CurrentFaceCaptureData { get; private set; }

        Thread _udpReadThread;
        UdpClient _udpClient;
        IPEndPoint _ipEndPoint;

        private ConcurrentQueue<FaceCaptureData> _receivedDataQueue = new ConcurrentQueue<FaceCaptureData>();

        private float _lastMessgaeTimestamp = 0;

        #region Unity Messages
        private void Start()
        {
            _Init();
        }

        private void OnDestroy()
        {
            _udpReadThread?.Abort();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                _blendshapeByteOffset += 1;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                _blendshapeByteOffset -= 1;
            }

            if (_receivedDataQueue.IsEmpty)
            {
                if (Time.time - _lastMessgaeTimestamp > 1)
                {
                    CurrentFaceCaptureData = null;
                }
                return;
            }

            _lastMessgaeTimestamp = Time.time;

            FaceCaptureData blendshapeData = default;

            while (_receivedDataQueue.TryDequeue(out blendshapeData))
            {
                CurrentFaceCaptureData = blendshapeData;
                receivedFaceCaptureData?.Invoke(blendshapeData);
            }
        }

        private void OnGUI()
        {
            if(_debugPrint)
            {
                string _debugBlendshapeInfo = "Current Blendshape Info:" + Environment.NewLine;
                GUIStyle label = new GUIStyle();
                label.fontSize = 40;

                if (CurrentFaceCaptureData != null)
                {
                    for (int i = 0; i < CurrentFaceCaptureData.blendshapeValues.Count; i++)
                    {
                        GUI.Label(new Rect(10 + (i / 31) * 300, 10 + (i / 31 + i % 31) * 30, 300, 30), $" {_ueLiveLinkDataPatch[i]}  {CurrentFaceCaptureData.blendshapeValues[i].ToString("0.000")}");
                    }

                    GUI.Label(new Rect(600, 320, 300, 30), $"HeadRotation {CurrentFaceCaptureData.headRotation}");
                    GUI.Label(new Rect(600, 350, 300, 30), $"LeftEyeRotation {CurrentFaceCaptureData.leftEyeRotation}");
                    GUI.Label(new Rect(600, 380, 300, 30), $"RightEyeRotation {CurrentFaceCaptureData.rightEyeRotation}");
                }
            }
        }

        #endregion

        private void _Init()
        {
            _udpClient = new UdpClient(_port);
            _ipEndPoint = new IPEndPoint(IPAddress.Any, 0);

            _udpReadThread = new Thread( new ThreadStart(_ReceiveData));
            _udpReadThread.IsBackground = true;
            _udpReadThread.Start();
        }

        private void _ReceiveData()
        {
            while (true)
            {
                try
                {
                    byte[] data = _udpClient.Receive(ref _ipEndPoint);

                    if (data.Length < _blendshapeCount * 4)
                    {
                        continue;
                    }

                    int byteSkipOffset = _blendshapeByteOffset;
                    if (_flipEndianness)
                    {
                        Array.Reverse(data);
                    }
                    var ueLiveLinkDataPatch = new float[_blendshapeCount];
                    Buffer.BlockCopy(data, _blendshapeByteOffset, ueLiveLinkDataPatch, 0, _blendshapeCount * 4);

                    if (_flipEndianness)
                    {
                        Array.Reverse(ueLiveLinkDataPatch);
                    }

                    _receivedDataQueue.Enqueue(new FaceCaptureData(ueLiveLinkDataPatch));
                }
                catch (Exception err)
                {
                    print(err.ToString());
                }
            }
        }

        // https://stackoverflow.com/questions/2871/reading-a-c-c-data-structure-in-c-sharp-from-a-byte-array
        T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            T stuff;
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            try
            {
                stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            }
            finally
            {
                handle.Free();
            }
            return stuff;
        }

        private static byte[] NetworkToHostOrder(byte[] array, int offset, int length)
        {
            return array.Skip(offset).Take(length).Reverse().ToArray();
        }
    }
}

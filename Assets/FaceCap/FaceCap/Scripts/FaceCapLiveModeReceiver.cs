﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FortySevenE;

public class FaceCapLiveModeReceiver : MonoBehaviour
{
    [SerializeField]
    public UnrealLiveLinkReceiver liveLinkReceiver;
    [SerializeField]
    public GameObject blendshapeMesh;
    [SerializeField]
    public int[] blendShapeIndexes;
    [SerializeField]
    public SkinnedMeshRenderer smr;

    [SerializeField]
    public bool usePositionData = false;
    Vector3 _startPosition = new Vector3(0, 0, 0);

    [SerializeField]
    public bool useRotationData = false;

    [SerializeField]
    public Transform headTransform;
    Quaternion _headRotationInitial;
    Quaternion _headRotationOffset;

    [SerializeField]
    public bool neckTransformEnabled = false;
    [SerializeField]
    public Transform neckTransform;
    [SerializeField]
    public float neckTransformBlendFactor = 0.66f;
    Quaternion _neckRotationInitial;
    Quaternion _neckRotationOffset;

    [SerializeField]
    public bool spineTransformEnabled = false;
    [SerializeField]
    public Transform spineTransform;
    [SerializeField]
    public float spineTransformBlendFactor = 0.33f;
    Quaternion _spineRotationInitial;
    Quaternion _spineRotationOffset;

    [SerializeField]
    public bool useEyeDirectionData = false;

    [SerializeField]
    public Transform leftEyeTransform;
    Quaternion _leftEyeRotationOffset;

    [SerializeField]
    public Transform rightEyeTransform;
    Quaternion _rightEyeRotationOffset;

    bool isEveryThingConfigured = true;

    private void OnEnable()
    {
        liveLinkReceiver.receivedFaceCaptureData += ApplyUeLiveLinkData;
    }

    private void OnDisable()
    {
        liveLinkReceiver.receivedFaceCaptureData -= ApplyUeLiveLinkData;
    }

    private void ApplyUeLiveLinkData(FaceCaptureData faceCaptureData)
    {
        for (int skinnedMeshBlendshapeIndex = 0; skinnedMeshBlendshapeIndex < blendShapeIndexes.Length; skinnedMeshBlendshapeIndex++)
        {
            smr.SetBlendShapeWeight(skinnedMeshBlendshapeIndex, faceCaptureData.blendshapeValues[blendShapeIndexes[skinnedMeshBlendshapeIndex]] * 100f);
        }

        if (useRotationData)
        {
            if (headTransform) headTransform.rotation = _headRotationInitial * faceCaptureData.headRotation;
            if (rightEyeTransform) rightEyeTransform.rotation = faceCaptureData.rightEyeRotation;
            if (leftEyeTransform) leftEyeTransform.rotation = faceCaptureData.leftEyeRotation;
        }
    }

    void Start()
    {

        if (blendshapeMesh == null)
        {
            isEveryThingConfigured = false;
            Debug.LogWarning("Face Cap Live Mode Receiver Error : Blenshape mesh is not assigned.");
        }
        else
        {
            smr = blendshapeMesh.GetComponent<SkinnedMeshRenderer>();
            if (smr == null)
            {
                isEveryThingConfigured = false;
                Debug.LogWarning("Face Cap Live Mode Receiver Error : Blenshape mesh has no blendshapes.");
            }
            else
            {
                if (smr.sharedMesh.blendShapeCount == 0)
                {
                    isEveryThingConfigured = false;
                    Debug.LogWarning("Face Cap Live Mode Receiver Error : Blenshape mesh has no blendshapes.");
                }
            }
        }

        if (usePositionData || useRotationData)
        {
            if (headTransform == null)
            {
                isEveryThingConfigured = false;
                Debug.LogWarning("Face Cap Live Mode Receiver Error : Head transform is not assigned.");
            }
            else
            {
                _headRotationInitial = headTransform.rotation;
                _headRotationOffset = Quaternion.Inverse(ConvertScneneKitSpaceToUnitySpace(new Vector3(0, 0, 0))) * headTransform.rotation;
                _startPosition = headTransform.localPosition;
            }

            if (neckTransformEnabled)
            {
                if (neckTransform == null)
                {
                    isEveryThingConfigured = false;
                    Debug.LogWarning("Face Cap Live Mode Receiver Error : Neck transform is not assigned.");
                }
                else
                {
                    _neckRotationInitial = neckTransform.rotation;
                    _neckRotationOffset = Quaternion.Inverse(ConvertScneneKitSpaceToUnitySpace(new Vector3(0, 0, 0))) * neckTransform.rotation;
                    _startPosition = neckTransform.localPosition;
                }
            }

            if (spineTransformEnabled)
            {
                if (spineTransform == null)
                {
                    isEveryThingConfigured = false;
                    Debug.LogWarning("Face Cap Live Mode Receiver Error : Spine transform is not assigned.");
                }
                else
                {
                    _spineRotationInitial = spineTransform.rotation;
                    _spineRotationOffset = Quaternion.Inverse(ConvertScneneKitSpaceToUnitySpace(new Vector3(0, 0, 0))) * spineTransform.rotation;
                    _startPosition = spineTransform.localPosition;
                }
            }
        }

        if (useEyeDirectionData)
        {
            if (leftEyeTransform == null && rightEyeTransform == null)
            {
                isEveryThingConfigured = false;
                Debug.LogWarning("Face Cap Live Mode Receiver Error : Assign at least 1 eye transform.");
            }

            if (leftEyeTransform != null)
            {
                _leftEyeRotationOffset = Quaternion.Inverse(ConvertScneneKitSpaceToUnitySpace(new Vector3(0, 0, 0))) * leftEyeTransform.rotation;
            }

            if (rightEyeTransform != null)
            {
                _rightEyeRotationOffset = Quaternion.Inverse(ConvertScneneKitSpaceToUnitySpace(new Vector3(0, 0, 0))) * rightEyeTransform.rotation;
            }
        }

        if (!isEveryThingConfigured)
        {
            return;
        }
    }

    protected Quaternion ConvertScneneKitSpaceToUnitySpace(Vector3 eulerAngles)
    {
        Quaternion q = Quaternion.Euler(eulerAngles);
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

}